import csv


def read_data(file_path):
    data = []
    with open(file_path, "r", encoding="utf-8") as f:
        for line in f:
            data.append(line.strip())
    return data


train_en = read_data("train/train.en")
train_vi = read_data("train/train.vi")
dev_en = read_data("dev/dev.en")
dev_vi = read_data("dev/dev.vi")
test_en = read_data("test/test.en")
test_vi = read_data("test/test.vi")

train_en += dev_en
train_vi += dev_vi
print(len(train_vi))
print(len(test_vi))

with open("test_parallel.tsv", "w", encoding="utf-8") as f:
    tsv_test = csv.writer(f, delimiter='\t', lineterminator='\n')
    for en, vi in zip(test_en, test_vi):
        tsv_test.writerow([en, vi])

with open("train_parallel.tsv", "w", encoding="utf-8") as f:
    tsv_train = csv.writer(f, delimiter='\t', lineterminator='\n')
    for en, vi in zip(train_en, train_vi):
        tsv_train.writerow([en, vi])
