import os
import string
import logging
import random
import numpy as np
import torch
import json
from vncorenlp import VnCoreNLP

dir_file_path = os.path.dirname(os.path.realpath(__file__))

# To perform word segmentation
vncorenlp_path = os.path.join(dir_file_path, "vncorenlp", "VnCoreNLP-1.1.1.jar")

annotator = VnCoreNLP(vncorenlp_path, annotators="wseg", max_heap_size='-Xmx500m')

# Load stop word
vi_stop_word = ["như", "làm", "là", "và", "với", "nếu", "thì", "do", "ở", "đây", "đó", "lại", "không",
                "cho", "chung", "đã", "nơi", "để", "đến", "số", "một", "khác", "được", "vào", "ra", "trong",
                "người", "loài", "từ", "nào", "bằng", "rằng", "nên", "gì", "ông", "bà", "anh", "cô", "việc", 
                "khi", "này", "chỉ", "về", "các", "còn", "trên", "những", "có", "mà", "nhưng", "nhiều", "nó",
                "sẽ", "của", "chưa", "lúc", "có_thể", "bởi_vì", "tại_vì", "như_thế", "thế_là", "trong_khi",
                "chẳng_hạn", "do_đó", "tuy_nhiên", "đôi_khi", "chỉ_là", "một_số", "chúng_nó", "rằng_là"]


number = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]
chars = ["a", "b", "c", "d", "đ", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "”", "“"]
stop_word_bm25 = number + chars + vi_stop_word


def init_logger():
    logging.basicConfig(
        format="%(asctime)s - %(levelname)s - %(name)s -   %(message)s",
        datefmt="%m/%d/%Y %H:%M:%S",
        level=logging.INFO,
    )


def set_seed(args):
    random.seed(args.seed)
    np.random.seed(args.seed)
    torch.manual_seed(args.seed)
    if not args.no_cuda and torch.cuda.is_available():
        torch.cuda.manual_seed_all(args.seed)


def bi_gram_split(tokens):
    bi_gram = tokens
    bi_gram += [" ".join(tokens[i:i+2]) for i in range(len(tokens) - 1)]
    return bi_gram

def word_tokenize(text):
    text = text.strip()
    text_tokens = annotator.tokenize(text)
    flat_tokens = [token for tokens in text_tokens for token in tokens]
    return flat_tokens


def remove_stopword_bm25(w):
    return w not in stop_word_bm25


def remove_punctuation(w):
    return w not in string.punctuation


def lower_case(w):
    return w.lower()


def bm25_tokenizer(text, use_bi=False, vi_tokenize=True):
    if vi_tokenize:
        tokens = word_tokenize(text)
    else:
        tokens = text.split()
    tokens = list(map(lower_case, tokens))
    tokens = list(filter(remove_punctuation, tokens))
    if use_bi:
        tokens = bi_gram_split(tokens)
    tokens = list(filter(remove_stopword_bm25, tokens))
    return tokens


def load_json(path):
    return json.load(open(path, "r", encoding="utf-8"))


if __name__ == "__main__":
    print(bm25_tokenizer("Ông là học trò, cộng sự của Chủ tịch Hồ Chí Minh", use_bi=True))