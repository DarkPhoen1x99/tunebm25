import json
from sentence_transformers import SentenceTransformer, util
import torch
import os


def load_json(path):
    return json.load(open(path, "r", encoding="utf-8"))


def load_train_data(train_path):
    data = load_json(train_path)
    questions = []
    postive_answers = []
    negative_answers = []
    for triplet in data:
        questions.append(triplet["question"])
        postive_answers.append(triplet["positive_answer"])
        negative_answers.append(triplet["negative_answer"])
    
    return questions, postive_answers, negative_answers


def load_corpus(corpus_path):
    corpus = []
    data = load_json(corpus_path)
    for article in data:
        title = article["title"]
        for context in article["contexts"]:
            corpus.append(title + ", " + context)
    return corpus


if __name__ == "__main__":

    bi_encoder = SentenceTransformer('bi_checkpoint')
    embeddings_filepath = "corpus_embedd.pt"
    corpus = load_corpus("corpus.json")
    if not os.path.exists(embeddings_filepath):
        corpus_embeddings = bi_encoder.encode(corpus, convert_to_tensor=True, show_progress_bar=True)
        torch.save(corpus_embeddings, "corpus_embedd.pt")
    else:
        # Load corpus_embedd by bi-enocder
        corpus_embeddings = torch.load(embeddings_filepath)
        corpus_embeddings = corpus_embeddings.float()  # Convert embedding file to float
    if torch.cuda.is_available():
        corpus_embeddings = corpus_embeddings.to('cuda')
    
    questions, positive_answers, negative_answers = load_train_data("test.json")
    question_embeddings = bi_encoder.encode(questions, convert_to_tensor=True)
    if torch.cuda.is_available():
        question_embeddings = question_embeddings.cuda()
   
    hits = util.semantic_search(question_embeddings, corpus_embeddings, top_k=20)
   
    negatives_from_bi = []
    for hit, actual_answer, negatives in zip(hits, positive_answers, negative_answers):
        negative_from_bi = []
        for id, candidate in enumerate(hit): 
            corpus_id = candidate["corpus_id"] 
            pred = corpus[corpus_id]
            if pred == actual_answer or pred in negatives[:5]:
                continue
            else:
                negative_from_bi.append(pred)
        negatives_from_bi.append(negative_from_bi)
    negatives_mining = [neg_bm25[:5] + neg_model[:5] for neg_bm25, neg_model in zip(negative_answers, negatives_from_bi)]

    qna_triplets = []
    for question, positive, negative in zip(questions, positive_answers, negatives_mining):
        triplet = {"question": question, "positive_answer": positive, "negative_answer": negative}
        qna_triplets.append(triplet)
    
    with open("triplets_mining_train.json", "w", encoding="utf-8") as f:
        json.dump(qna_triplets, f)