import json
from sentence_transformers import SentenceTransformer, util
from rank_metrics import mean_average_precision, recall_at_k, mean_reciprocal_rank
import torch
import os


def load_json(path):
    return json.load(open(path, "r", encoding="utf-8"))


def load_test_data(test_path):
    data = load_json(test_path)
    questions = []
    answers = []
    for article in data:
        title = article["title"]
        for qa in article["qas"]:
            if qa["label"] is True:
                questions.append(qa["question"])
                answers.append(title + ", " + qa["answer"])
    return questions, answers


def load_corpus(corpus_path):
    corpus = []
    data = load_json(corpus_path)
    for article in data:
        title = article["title"]
        for context in article["contexts"]:
            corpus.append(title + ", " + context)
    return corpus


if __name__ == "__main__":

    bi_encoder = SentenceTransformer('bi_checkpoint')
    embeddings_filepath = "corpus_embedd.pt"
    corpus = load_corpus("corpus.json")
    if not os.path.exists(embeddings_filepath):
        corpus_embeddings = bi_encoder.encode(corpus, convert_to_tensor=True, show_progress_bar=True)
        torch.save(corpus_embeddings, "corpus_embedd.pt")
    else:
        # Load corpus_embedd by bi-enocder
        corpus_embeddings = torch.load(embeddings_filepath)
        corpus_embeddings = corpus_embeddings.float()  # Convert embedding file to float
    if torch.cuda.is_available():
        corpus_embeddings = corpus_embeddings.to('cuda')
    
    questions, answers = load_test_data("test.json")
    question_embeddings = bi_encoder.encode(questions, convert_to_tensor=True)
    if torch.cuda.is_available():
        question_embeddings = question_embeddings.cuda()
    print("Start evaluate !!!")
    hits = util.semantic_search(question_embeddings, corpus_embeddings, top_k=100)
    print("Compute metric !!!")
    res_queries = []
    for hit, actual_answer in zip(hits, answers):
        binary_pred_res = [0] * 100
        for id, candidate in enumerate(hit): 
            corpus_id = candidate["corpus_id"] 
            pred = corpus[corpus_id]
            if pred == actual_answer:
                binary_pred_res[id] = 1
                break
        res_queries.append(binary_pred_res)

    print(f"Testset has {len(questions)} questions !!!")
    actual_positive = [1] * len(questions) # 1 for just 1 relevant document per query
            
    recall_1 = recall_at_k([r[:1] for r in res_queries], actual_positive) * 100
    recall_10 = recall_at_k([r[:10] for r in res_queries], actual_positive) * 100
    recall_100 = recall_at_k(res_queries, actual_positive) * 100
    map_maxk = mean_average_precision(res_queries) * 100
    mrr_maxk = mean_reciprocal_rank(res_queries) * 100
    print("Recall@1: {} %".format(recall_1))
    print("Recall@10: {} %".format(recall_10))
    print("Recall@{}: {} %".format(100, recall_100))
    print("MAP@{}: {} %".format(100, map_maxk))
    print("MRR@{}: {} %".format(100, mrr_maxk))
    