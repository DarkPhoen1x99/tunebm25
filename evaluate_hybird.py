import json
from sentence_transformers import SentenceTransformer, util, LoggingHandler
from rank_metrics import mean_average_precision, recall_at_k, mean_reciprocal_rank
from hybird_search import semantic_search
from utils import bm25_tokenizer
from tqdm import tqdm
import time
import logging
import torch
import pickle
import os


def load_json(path):
    return json.load(open(path, "r", encoding="utf-8"))


def load_bm25(bm25_path):
    with open(bm25_path, "rb") as bm_file:
        bm25 = pickle.load(bm_file)
    return bm25


def load_test_data(test_path):
    data = load_json(test_path)
    questions = []
    answers = []
    for article in data:
        title = article["title"]
        for qa in article["qas"]:
            if qa["label"] is True:
                questions.append(qa["question"])
                answers.append(title + ", " + qa["answer"])
    return questions, answers


def get_bm25_scores(bm25, questions):
    print("Tokening questions !!!")
    bm25_questions = []
    for question in tqdm(questions):
        bm25_questions.append(bm25_tokenizer(question))
    bm25_scores = []
    for question in tqdm(bm25_questions):
        doc_scores = bm25.get_scores(question)
        bm25_scores.append(doc_scores)
    return bm25_scores


def load_corpus(corpus_path):
    corpus = []
    data = load_json(corpus_path)
    for article in data:
        title = article["title"]
        for context in article["contexts"]:
            corpus.append(title + ", " + context)
    return corpus


if __name__ == "__main__":

    logging.basicConfig(format='%(asctime)s - %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S',
                        level=logging.INFO,
                        handlers=[LoggingHandler()])
    logger = logging.getLogger(__name__)
    logger.info("Load bm25 !!!")
    bm25 = load_bm25("bm25_checkpoint/bm25_model")
    logger.info("Load bi model !!!")
    bi_encoder = SentenceTransformer('bi_checpoint-2022-06-01_08-12-54')
    embeddings_filepath = "corpus_embedd.pt"
    corpus = load_corpus("corpus.json")
    if not os.path.exists(embeddings_filepath):
        corpus_embeddings = bi_encoder.encode(corpus, convert_to_tensor=True, show_progress_bar=True)
        torch.save(corpus_embeddings, "corpus_embedd.pt")
    else:
        # Load corpus_embedd by bi-enocder
        corpus_embeddings = torch.load(embeddings_filepath)
        corpus_embeddings = corpus_embeddings.float()  # Convert embedding file to float
    if torch.cuda.is_available():
        corpus_embeddings = corpus_embeddings.to('cuda')
    
    questions, answers = load_test_data("test.json")
    bm25_scores = get_bm25_scores(bm25, questions)
    start_time = time.time()
    question_embeddings = bi_encoder.encode(questions, batch_size=200, device="cuda", convert_to_tensor=True)
    end_time = time.time()
    print("Encode questions done after {:.2f} seconds".format(end_time - start_time))
    if torch.cuda.is_available():
        start_time = time.time()
        question_embeddings = question_embeddings.cuda()
        end_time = time.time()
        print("Move to cuda done after {:.2f} seconds".format(end_time - start_time))
    logger.info("Start evaluate !!!")
    # hits = util.semantic_search(question_embeddings, corpus_embeddings, top_k=100)
    hits = semantic_search(question_embeddings, corpus_embeddings, bm25_scores, top_k=100)
    logger.info("Compute metric !!!")
    res_queries = []
    for hit, actual_answer in zip(hits, answers):
        binary_pred_res = [0] * 100
        for id, candidate in enumerate(hit): 
            corpus_id = candidate["corpus_id"] 
            pred = corpus[corpus_id]
            if pred == actual_answer:
                binary_pred_res[id] = 1
                break
        res_queries.append(binary_pred_res)

    logger.info(f"Dataset has {len(questions)} pairs !!!")
    actual_positive = [1] * len(questions) # 1 for just 1 relevant document per query
            
    recall_1 = recall_at_k([r[:1] for r in res_queries], actual_positive) * 100
    recall_20 = recall_at_k([r[:20] for r in res_queries], actual_positive) * 100
    recall_100 = recall_at_k(res_queries, actual_positive) * 100
    map_maxk = mean_average_precision(res_queries) * 100
    mrr_maxk = mean_reciprocal_rank(res_queries) * 100
    logger.info("Recall@1: {} %".format(recall_1))
    logger.info("Recall@20: {} %".format(recall_20))
    logger.info("Recall@{}: {} %".format(100, recall_100))
    logger.info("MAP@{}: {} %".format(100, map_maxk))
    logger.info("MRR@{}: {} %".format(100, mrr_maxk))
    