import math
from sentence_transformers import models, losses, datasets, util
from sentence_transformers import LoggingHandler, SentenceTransformer, InputExample
from sentence_transformers.evaluation import BinaryClassificationEvaluator
import logging
import os
import json
import datetime


#### Just some code to print debug information to stdout
logging.basicConfig(format='%(asctime)s - %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    level=logging.INFO,
                    handlers=[LoggingHandler()])

logger = logging.getLogger(__name__)
#### /print debug information to stdout

# Process data
train_data_path = "/kaggle/input/train-sbert-data/bm25_pairs_top10_train.json"
dev_data_path = "/kaggle/input/train-sbert-data/bm25_pairs_top1_dev.json"

with open(train_data_path, "r", encoding="utf-8") as f_train, open(dev_data_path, "r", encoding="utf-8") as f_dev:
    train_pairs = json.load(f_train)
    dev_pairs = json.load(f_dev)

train_samples = []
dev_samples = []

for pair in train_pairs:
    question = pair["question"]
    positive_answer = pair["positive_answer"]
    for negative_answer in pair["negative_answer"][:1]:
        train_samples.append(InputExample(texts=[question, positive_answer, negative_answer]))

logger.info(f"There are {len(train_samples)} pair sentences for training.")       
logger.info(f"Example for training input: {train_samples[0]}")
    
for pair in dev_pairs:
    question = pair["question"]
    positive_answer = pair["positive_answer"]
    # negative_answer = random.choice(pair["negative_answer"][1:])
    dev_samples.append(InputExample(texts=[question, positive_answer], label=1))
    dev_samples.append(InputExample(texts=[question, pair["negative_answer"][0]], label=0))
    # dev_samples.append(InputExample(texts=[question, negative_answer], label=0))

logger.info(f"There are {len(dev_samples)} pair sentences for evaluate.")    
logger.info(f"example for evaluate input: {dev_samples[0]}")

# Define model
model_name = "vinai/phobert-base"
train_batch_size = 32   # The larger you select this, the better the results (usually). But it requires more GPU memory
max_seq_length = 256
num_epochs = 4
round_train = 2

if round_train > 1:
    logger.info("Load pretrained model")
    model = SentenceTransformer(model_name)
else:
    # Here we define our SentenceTransformer model
    logger.info("Create model from scratch")
    word_embedding_model = models.Transformer(model_name, max_seq_length=max_seq_length)
    pooling_model = models.Pooling(word_embedding_model.get_word_embedding_dimension(), pooling_mode='mean')
    model = SentenceTransformer(modules=[word_embedding_model, pooling_model])

# Save path of the model
model_save_path = "bi_checpoint-" + datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
os.makedirs(model_save_path, exist_ok=True)

# Special data loader that avoid duplicates within a batch
train_dataloader = datasets.NoDuplicatesDataLoader(train_samples, batch_size=train_batch_size)

# Our training loss
train_loss = losses.MultipleNegativesRankingLoss(model)

# During training, we use CESoftmaxAccuracyEvaluator to measure the accuracy on the dev set.
evaluator = BinaryClassificationEvaluator.from_input_examples(dev_samples, name='dev')

# Configure the training
warmup_steps = math.ceil(len(train_dataloader) * num_epochs * 0.1) # 10% of train data for warm-up
logging.info("Warmup-steps: {}".format(warmup_steps))


# Train the model
model.fit(train_objectives=[(train_dataloader, train_loss)],
          evaluator=evaluator,
          epochs=num_epochs,
          evaluation_steps=int(len(train_dataloader)*0.1),
          warmup_steps=warmup_steps,
          output_path=model_save_path,
          use_amp=False
          )