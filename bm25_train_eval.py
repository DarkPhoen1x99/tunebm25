from tqdm import tqdm
from rank_bm25 import BM25Plus, BM25Okapi
from utils import bm25_tokenizer, load_json, init_logger
import argparse
import logging
import pickle
import os
from rank_metrics import recall_at_k, mean_average_precision, mean_reciprocal_rank


init_logger()
logger = logging.getLogger(__name__)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--k1", default=0.82, type=float)
    parser.add_argument("--b", default=0.68, type=float)
    parser.add_argument("--use_plus", default=True, type=bool)
    parser.add_argument("--top_maxk", default=100, type=int)
    parser.add_argument("--use_bigram", default=False, type=bool)
    parser.add_argument("--vi_tokenize", default=True, type=bool)
    parser.add_argument("--save_path", default="bm25_checkpoint", type=str)
    args = parser.parse_args()

    logger.info(args)

    corpus = load_json("data/corpus.json")

    os.makedirs(args.save_path, exist_ok=True)
    logger.info("Process documents !!!")
    num_document = 0
    documents = []
    doc_refers = []
    for article in tqdm(corpus):
        article_title = article["title"]
        article_paragraphs = article["contexts"]
        num_document += len(article_paragraphs)
        for sentence in article_paragraphs:
            doc_refers.append([article_title, sentence])
            sent_full = article_title + ", " + sentence
            tokens = bm25_tokenizer(sent_full, args.use_bigram, args.vi_tokenize)
            documents.append(tokens)
    logger.info(f"Number of documents: {num_document} !!!")
    with open(os.path.join(args.save_path, "documents_manual"), "wb") as documents_file:
            pickle.dump(documents, documents_file)
    if args.use_plus:
        logger.info("Train and evalute BM25Plus")
        bm25 = BM25Plus(documents, k1=args.k1, b=args.b)
    else:
        logger.info("Train and evalute BM25Okapi")
        bm25 = BM25Okapi(documents, k1=args.k1, b=args.b)
    
    with open(os.path.join(args.save_path, "bm25_model"), "wb") as bm_file:
        pickle.dump(bm25, bm_file)
    
    logger.info("Load evaluate dataset !!!")
    eval_data = load_json("data/test.json")
    actual_positive = 0
    logger.info("evaluate !!!")
    res_queries = []
    for article in tqdm(eval_data):
        actual_positive += len(article["qas"])
        for qa in article["qas"]:
            question = qa["question"]
            tokenized_query = bm25_tokenizer(question, args.use_bigram, args.vi_tokenize)
            doc_scores = bm25.get_scores(tokenized_query)
            predictions = doc_scores.argsort()[::-1][:args.top_maxk]
            assert len(predictions) == args.top_maxk
            binary_pred_res = [0] * args.top_maxk
            for id, idx_pred in enumerate(predictions): 
                pred = doc_refers[idx_pred]
                if pred[1] == qa["answer"]: 
                    binary_pred_res[id] = 1
                    break # break here because 1 query just have 1 relevant, comment this line if you have more relevant per query 
            res_queries.append(binary_pred_res)
    
    logger.info("Dataset has %d pairs !!!", actual_positive)
    actual_positive = [1] * actual_positive # 1 for just 1 relevant document per query
            
    recall_1 = recall_at_k([r[:1] for r in res_queries], actual_positive) * 100
    recall_10 = recall_at_k([r[:10] for r in res_queries], actual_positive) * 100
    recall_100 = recall_at_k(res_queries, actual_positive) * 100
    map_maxk = mean_average_precision(res_queries) * 100
    mrr_maxk = mean_reciprocal_rank(res_queries) * 100
    logger.info(f"BM25 use k1 = {args.k1}, b = {args.b}")
    logger.info("Recall@1: {} %".format(recall_1))
    logger.info("Recall@10: {} %".format(recall_10))
    logger.info("Recall@{}: {} %".format(args.top_maxk, recall_100))
    logger.info("MAP@{}: {} %".format(args.top_maxk, map_maxk))
    logger.info("MRR@{}: {} %".format(args.top_maxk, mrr_maxk))
    logger.info("Train & Evalute BM25 done !!!")