from sentence_transformers import SentenceTransformer, LoggingHandler, models, evaluation, losses
from torch.utils.data import DataLoader
from sentence_transformers.datasets import ParallelSentencesDataset
from datetime import datetime
import os
import logging
import numpy as np


logging.basicConfig(format='%(asctime)s - %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    level=logging.INFO,
                    handlers=[LoggingHandler()])
logger = logging.getLogger(__name__)


teacher_model_name = 'multi-qa-distilbert-dot-v1'   # Our monolingual teacher model, we want to convert to multiple languages
student_model_name = 'xlm-roberta-base'       # Multilingual base model we use to imitate the teacher model

max_seq_length = 128                 # Student model max. lengths for inputs (number of word pieces)
train_batch_size = 32                # Batch size for training
inference_batch_size = 32            # Batch size at inference
max_sentences_training = 2000000     # Maximum number of  parallel sentences for training
train_max_sentence_length = 250      # Maximum length (characters) for parallel training sentences

num_epochs = 5                       # Train for x epochs
num_warmup_steps = 10000             # Warumup steps

num_evaluation_steps = 5000          # Evaluate performance after every xxxx steps



output_path = "/kaggle/working/checpoint-parallel-" + datetime.now().strftime("%Y-%m-%d_%H-%M-%S")

######## Start the extension of the teacher model to multiple languages ########
logger.info("Load teacher model")
teacher_model = SentenceTransformer(teacher_model_name)


logger.info("Create student model from scratch")
word_embedding_model = models.Transformer(student_model_name, max_seq_length=max_seq_length)
# Apply mean pooling to get one fixed sized sentence vector
pooling_model = models.Pooling(word_embedding_model.get_word_embedding_dimension())
student_model = SentenceTransformer(modules=[word_embedding_model, pooling_model])


###### Read Parallel Sentences Dataset ######
train_file = "/kaggle/input/parallel-dataset/train_parallel.tsv"
train_data = ParallelSentencesDataset(student_model=student_model, teacher_model=teacher_model, batch_size=inference_batch_size, use_embedding_cache=True)

train_data.load_data(train_file, max_sentences=max_sentences_training, max_sentence_length=train_max_sentence_length)
logger.info(f"Train data has: {len(train_data) // 2} pairs")

train_dataloader = DataLoader(train_data, shuffle=True, batch_size=train_batch_size)
train_loss = losses.MSELoss(model=student_model)

#### Evaluate cross-lingual performance on different tasks #####
dev_file = "/kaggle/input/parallel-dataset/test_parallel.tsv"
evaluators = []         # evaluators has a list of different evaluator classes we call periodically


logger.info("Create evaluator for " + dev_file)
src_sentences = []
trg_sentences = []
with open(dev_file, "r", encoding="utf-8") as fIn:
    for line in fIn:
        splits = line.strip().split('\t')
        if len(splits) < 2:
            print(splits)
        if splits[0] != "" and splits[1] != "":
            src_sentences.append(splits[0])
            trg_sentences.append(splits[1])

logger.info(f"Evaluation data has: {len(src_sentences)} pairs")
logger.info(f"Example evaluation data: {src_sentences[0]}, {trg_sentences[0]}")

# Mean Squared Error (MSE) measures the (euclidean) distance between teacher and student embeddings
dev_mse = evaluation.MSEEvaluator(src_sentences, trg_sentences, name=os.path.basename(dev_file), teacher_model=teacher_model, batch_size=inference_batch_size)
evaluators.append(dev_mse)

# TranslationEvaluator computes the embeddings for all parallel sentences. It then check if the embedding of source[i] is the closest to target[i] out of all available target sentences
dev_trans_acc = evaluation.TranslationEvaluator(src_sentences, trg_sentences, name=os.path.basename(dev_file),batch_size=inference_batch_size)
evaluators.append(dev_trans_acc)



# Train the model
student_model.fit(train_objectives=[(train_dataloader, train_loss)],
          evaluator=evaluation.SequentialEvaluator(evaluators, main_score_function=lambda scores: np.mean(scores)),
          epochs=num_epochs,
          warmup_steps=num_warmup_steps,
          evaluation_steps=num_evaluation_steps,
          output_path=output_path,
          save_best_model=True,
          optimizer_params= {'lr': 2e-5, 'eps': 1e-6, 'correct_bias': False}
          )